# README #

### What is this repository for? ###

* Quick summary
This project is the result of a frontend assignment for [Cabana](https://www.cabana.dk).

* Version
Version: 1.0.0

### How do I get set up? ###

- Download and install npm
* Dependencies
	- npm
	- (bower - for further development)
* Database configuration
	- (none)
* How to run tests
	- (no tests)
* Deployment instructions
	* `cd` into project directory.
	* run `npm install`
	* run `gulp`
	* open browser and visit `http://localhost:8080/`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
- Author: Mike Hynne <mikehynne@gmail.com>
* Other community or team contact